# gitlab_proof

[Verifying my OpenPGP key: openpgp4fpr:b498e2e410902f8aec108f4f5bdc557b496bdb0d]

This is an OpenPGP proof that connects my OpenPGP key to this GitLab account. For details check out https://keyoxide.org/guides/openpgp-proofs

